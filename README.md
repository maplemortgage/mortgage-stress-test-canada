Functions for evaluating the pass or fail result for a mortgage stress test. Used in MapleMortgage.ca's stress test calculator, https://www.maplemortgage.ca/mortgage-stress-test-calculator/

All banks that are federally regulated must perform a mortgage stress test on all prospective borrows. Credit unions and mortgage finance companies are not required to perform a mortgage stress test but often do as a precaution. 

For insured mortgages (those with less than 20% down payment), the stress test must be performed with the higher of the Bank of Canada qualifying interest rate (5.19%) or the contracted mortgage rate + 2%.

For mortgages that do not need mortgage default insurance (those with 20% or greater down payment), the stress test should be performed using the Bank of Canada qualifying rate (5.19%) or the contracted mortgage rates, whichever is higher.

To pass the mortgage stress test, an applicant must exceed the set Gross Debt Service and Total Debt Service ratios. The Canadian Mortgage and Housing Corporation recommend that the minimum GDS and TDS ratios that must be met are 35% and 42% respectively. 


