/*
 * Example of how to compute the result of a mortgage stress test
 */

var result = evaluateStressTestPassFail(
    95000, //income
    0, //monthly rental income
    200, //monthly heating cost
    100, //monthly condo fees
    3000, //yearly property tax
    5500, //credit card balance
    0, //unsecured line of credit balance
    350, //monthly car payment
    0, //other monthly secured debt payments
    1500, //mortgage payment
    0.5, //rental income offset (most lenders use 50%)
    0, //mortgage payment frequency - 0 is for monthly payments
    0.35, //gross debt service ratio threshold (35%)
    0.42 //total debt service ratio threshold (42%)
    );

console.log("Did pass mortgage stress test? "+result)

/*
 * Called to calculate the pass or fail status for the mortgage stress test
 */
function evaluateStressTestPassFail(
    annualIncome, 
    monthlyRentalIncome,
    monthlyHeatingCost,
    monthlyCondoFees,
    yearlyPropertyTax,
    creditCardBalance,
    unsecuredLocBalance, 
    monthlyCarPayment,
    monthlyOtherDebtPayment,
    mortgagePayment,
    rentalIncomeOffsetThreshold, //Usually 0.5 (50%)
    paymentFrequencyType, //0 is monthly, 1 is weekly, 2 is acc.weekly, 3 is bi-weekly, 4 is acc.biweekly, 5 is semi-monthly
    gdsThreshold,
    tdsThreshold,
    )
{ 
    //If a value is not a valid number, set it to 0
    if(isNaN(annualIncome) || annualIncome < 0) annualIncome = 0;
    if(isNaN(monthlyRentalIncome) || monthlyRentalIncome < 0) monthlyRentalIncome = 0;
    if(isNaN(monthlyHeatingCost) || monthlyHeatingCost < 0) monthlyHeatingCost = 0;
    if(isNaN(monthlyCondoFees) || monthlyCondoFees < 0) monthlyCondoFees = 0;
    if(isNaN(yearlyPropertyTax) || yearlyPropertyTax < 0) yearlyPropertyTax = 0;
    if(isNaN(creditCardBalance) || creditCardBalance < 0) creditCardBalance = 0;
    if(isNaN(monthlyCarPayment) || monthlyCarPayment < 0) monthlyCarPayment = 0;
    if(isNaN(monthlyOtherDebtPayment) || monthlyOtherDebtPayment < 0) monthlyOtherDebtPayment = 0;
    if(isNaN(mortgagePayment) || mortgagePayment < 0) mortgagePayment = 0;
    if(isNaN(rentalIncomeOffsetThreshold) || rentalIncomeOffsetThreshold < 0) rentalIncomeOffsetThreshold = 0;
    if(isNaN(paymentFrequencyType) || paymentFrequencyType < 0) paymentFrequencyType = 0;
    if(isNaN(gdsThreshold) || gdsThreshold < 0) gdsThreshold = 0;
    if(isNaN(tdsThreshold) || tdsThreshold < 0) tdsThreshold = 0;

    //Calculate the average monthly mortgage payment outflow
    var monthlyAverageMortgageOutFlow = 0;
    if(paymentFrequencyType == 0) monthlyAverageMortgageOutFlow = Number(mortgagePayment); //Monthly
    else if(paymentFrequencyType == 1) monthlyAverageMortgageOutFlow = ((Number(mortgagePayment) * 52) /  12); //Weekly
    else if(paymentFrequencyType == 2) monthlyAverageMortgageOutFlow = ((Number(mortgagePayment) * 52) /  12); //Acc Weekly
    else if(paymentFrequencyType == 3) monthlyAverageMortgageOutFlow = ((Number(mortgagePayment) * 26) /  12); //Bi Weekly
    else if(paymentFrequencyType == 4) monthlyAverageMortgageOutFlow = ((Number(mortgagePayment) * 26) /  12); //Acc Biweekly
    else if(paymentFrequencyType == 5) monthlyAverageMortgageOutFlow = ((Number(mortgagePayment) * 24) /  12); //Semi Monthly
    else monthlyAverageMortgageOutFlow = Number(mortgagePayment);

    //Calulate gross debt service ratio
    var monthlyIncome = (annualIncome / 12) + (monthlyRentalIncome * rentalIncomeOffsetThreshold);
    var propertyCosts = (monthlyAverageMortgageOutFlow + monthlyHeatingCost + (monthlyCondoFees * 0.5) + (yearlyPropertyTax / 12));
    var gds = propertyCosts / monthlyIncome;
    if(isNaN(gds) || Number(gds) > 99999999999999999999999999) gds = 0;

    //Calulate total debt service ratio
    var monthlyCreditCardPayment = creditCardBalance * 0.03; //Take 3% of CC balance as monthly payment
    var monthlyLOCPayment = unsecuredLocBalance * 0.03; //Take 3% of LOC balance as monthly payment
    var debtPayments = monthlyCreditCardPayment + monthlyLOCPayment+ monthlyCarPayment + monthlyOtherDebtPayment;
    var tds = (propertyCosts + debtPayments) / monthlyIncome;
    if(isNaN(tds) || Number(tds) > 99999999999999999999999999) tds = 0;

    //Stress test result
    var didPassStressTest = null;
    if(gds <= gdsThreshold && tds <= tdsThreshold) didPassStressTest = true;
    else didPassStressTest = false;

    return didPassStressTest;
}
